package com.yumaas.bus.home;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.yumaas.bus.R;

import com.yumaas.bus.base.UserPreferenceHelper;
import com.yumaas.bus.base.volleyutils.ConnectionHelper;
import com.yumaas.bus.base.volleyutils.ConnectionListener;
import com.yumaas.bus.home.response.ChildsResponse;
import com.yumaas.bus.noneed.FragmentHelper;
import com.yumaas.bus.noneed.ScannerActivity;



public class HomeFragment extends Fragment {

    View rootView;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_home, container, false);

        rootView.findViewById(R.id.btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
             }
        });


        rootView.findViewById(R.id.sccan).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivityForResult(new Intent(getActivity(), ScannerActivity.class),1234);
            }
        });


        getChildren();



        return rootView;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

//        UserDetailsFragment userDetailsFragment = new UserDetailsFragment();
//        Bundle bundle = new Bundle();
//        bundle.putString("code",data.getStringExtra("code"));
//        userDetailsFragment.setArguments(bundle);
//        FragmentHelper.replaceFragment(requireActivity(), userDetailsFragment, "CompanyDetailsFragment");

    }

    public void getChildren(){

        UsersRequest loginRequest = new UsersRequest("childs");
        loginRequest.setOwnerId(UserPreferenceHelper.getUserDetails().getId()+"");

        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                ChildsResponse childsResponse = (ChildsResponse)response;

                final ChildsAdapter teachersAdapter = new ChildsAdapter(getActivity(),childsResponse.getChilds());
                final RecyclerView programsList = rootView.findViewById(R.id.recycler_view);
                programsList.setLayoutManager(new GridLayoutManager(getActivity(),2));
                programsList.setAdapter(teachersAdapter);

            }

            @Override
            public void onRequestError(Object error) {

            }
        }).requestJsonObject(loginRequest, ChildsResponse.class);
    }
}