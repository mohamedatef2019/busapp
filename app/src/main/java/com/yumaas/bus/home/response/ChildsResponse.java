package com.yumaas.bus.home.response;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class ChildsResponse{

	@SerializedName("state")
	private int state;

	@SerializedName("childs")
	private List<ChildsItem> childs;

	public void setState(int state){
		this.state = state;
	}

	public int getState(){
		return state;
	}

	public void setChilds(List<ChildsItem> childs){
		this.childs = childs;
	}

	public List<ChildsItem> getChilds(){
		return childs;
	}
}