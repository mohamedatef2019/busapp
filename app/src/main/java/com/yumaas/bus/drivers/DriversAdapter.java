package com.yumaas.bus.drivers;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.RecyclerView;

import com.yumaas.bus.R;
import com.yumaas.bus.base.DefaultResponse;
import com.yumaas.bus.base.UserPreferenceHelper;
import com.yumaas.bus.base.volleyutils.ConnectionHelper;
import com.yumaas.bus.base.volleyutils.ConnectionListener;
import com.yumaas.bus.drivers.response.DriversItem;
import com.yumaas.bus.trips.BookTripRequest;

import java.util.ArrayList;
import java.util.List;


public class DriversAdapter extends RecyclerView.Adapter<DriversAdapter.ViewHolder> {

    Context context;

    List<DriversItem> driversItems;


    public DriversAdapter(Context context,List<DriversItem>driversItems) {
        this.context = context;
        this.driversItems=driversItems;
    }


    @Override
    public DriversAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_driver, parent, false);
        DriversAdapter.ViewHolder viewHolder = new DriversAdapter.ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(DriversAdapter.ViewHolder holder, final int position) {

        holder.name.setText("Name : "+driversItems.get(position).getName());
        holder.details.setText("Phone : "+driversItems.get(position).getPhone()+"\n"+
                "Email : "+driversItems.get(position).getEmail());


//        holder.itemView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//
//            }
//        });

        holder.delete.setOnClickListener(v -> {

            AlertDialog.Builder alert = new AlertDialog.Builder(context);

            alert.setMessage("You want to delete this user");
            alert.setTitle("Yes Delete");

            alert.setPositiveButton("Yes Delete", (dialog, whichButton) -> deleteDriver(position));

            alert.setNegativeButton("No Thanks", (dialog, whichButton) -> {
                dialog.dismiss();
                dialog.cancel();
            });

            alert.show();


        });

        holder.call.setOnClickListener(view -> {
            try {
                Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + driversItems.get(position).getPhone()));
                view.getContext().startActivity(intent);
            }catch (Exception e){
                e.getStackTrace();
            }
        });
    }


    public void deleteDriver(int position){

        BookTripRequest loginRequest = new BookTripRequest("company_delete_driver");
        loginRequest.setUserId(driversItems.get(position).getId()+"");


        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {

                DefaultResponse defaultResponse = (DefaultResponse) response;
                if(defaultResponse.getStatus()==101) {

                    Toast.makeText(context, "Deleted Successfully", Toast.LENGTH_SHORT).show();
                    driversItems.remove(position);
                    notifyDataSetChanged();
                }
            }

            @Override
            public void onRequestError(Object error) {

            }
        }).requestJsonObject(loginRequest, DefaultResponse.class);
    }

    @Override
    public int getItemCount() {
        return driversItems==null?0:driversItems.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        ImageView imageView;
        TextView name,details,call,delete;

        public ViewHolder(View itemView) {
            super(itemView);
            name=itemView.findViewById(R.id.name);
            details=itemView.findViewById(R.id.details);
            imageView=itemView.findViewById(R.id.image);
            call=itemView.findViewById(R.id.call);
            delete=itemView.findViewById(R.id.delete);

        }
    }
}
