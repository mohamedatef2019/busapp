package com.yumaas.bus.base;

import android.graphics.drawable.Drawable;

 import com.yumaas.bus.base.volleyutils.MyApplication;


public class ResourcesManager {

    public static Drawable getDrawable(int resId){
        return MyApplication.getInstance().getResources().getDrawable(resId);
    }

    public static int getColor(int resId){
        return MyApplication.getInstance().getResources().getColor(resId);
    }

    public static String getString(int resId){
        return MyApplication.getInstance().getResources().getString(resId);
    }
}
