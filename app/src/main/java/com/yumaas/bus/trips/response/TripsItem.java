package com.yumaas.bus.trips.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TripsItem{

	@SerializedName("number")
	private String number;

	@Expose
	@SerializedName("company_name")
	private String companyName;

	@SerializedName("trip_to")
	private String tripTo;

	@SerializedName("trip_from")
	private String tripFrom;

	@SerializedName("company_id")
	private String companyId;

	@SerializedName("driver")
	private String driver;

	@SerializedName("price")
	private String price;

	@SerializedName("name")
	private String name;

	@SerializedName("details")
	private String details;

	@SerializedName("comment")
	private String comment;

	@SerializedName("id")
	private String id;

	public void setNumber(String number){
		this.number = number;
	}

	public String getNumber(){
		return number;
	}

	public void setTripTo(String tripTo){
		this.tripTo = tripTo;
	}

	public String getTripTo(){
		return tripTo;
	}

	public void setTripFrom(String tripFrom){
		this.tripFrom = tripFrom;
	}

	public String getTripFrom(){
		return tripFrom;
	}

	public void setCompanyId(String companyId){
		this.companyId = companyId;
	}

	public String getCompanyId(){
		return companyId;
	}

	public void setDriver(String driver){
		this.driver = driver;
	}

	public String getDriver(){
		return driver;
	}

	public void setPrice(String price){
		this.price = price;
	}

	public String getPrice(){
		return price;
	}

	public void setName(String name){
		this.name = name;
	}

	public String getName(){
		return name;
	}

	public void setDetails(String details){
		this.details = details;
	}

	public String getDetails(){
		return details;
	}

	public void setComment(String comment){
		this.comment = comment;
	}

	public String getComment(){
		return comment;
	}

	public void setId(String id){
		this.id = id;
	}

	public String getId(){
		return id;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getCompanyName() {
		return companyName;
	}
}