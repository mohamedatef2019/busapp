package com.yumaas.bus.trips;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.RecyclerView;

import com.yumaas.bus.R;
import com.yumaas.bus.base.DefaultResponse;
import com.yumaas.bus.base.UserPreferenceHelper;
import com.yumaas.bus.base.volleyutils.ConnectionHelper;
import com.yumaas.bus.base.volleyutils.ConnectionListener;
import com.yumaas.bus.noneed.ScannerActivity;
import com.yumaas.bus.trips.response.TripsItem;

import java.util.List;


public class UserOldTripsAdapter extends RecyclerView.Adapter<UserOldTripsAdapter.ViewHolder> {

    Context context;
    String tripId;
    List<TripsItem> tripsItems;




    public UserOldTripsAdapter(Context context, List<TripsItem>tripsItems) {
        this.context = context;
        this.tripsItems=tripsItems;
    }


    @Override
    public UserOldTripsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_trip, parent, false);
        UserOldTripsAdapter.ViewHolder viewHolder = new UserOldTripsAdapter.ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(UserOldTripsAdapter.ViewHolder holder, final int position) {

        holder.name.setText(
                "Company Name : "+tripsItems.get(position).getCompanyName()
                        +"\n"+ "Driver Name : "+tripsItems.get(position).getDriver()
                        +"\n"+
                        "Trip Name : "+tripsItems.get(position).getName()
        );
        holder.details.setText("From : "+tripsItems.get(position).getTripFrom()+"\n"+
                "To   : "+tripsItems.get(position).getTripTo()

                +"\n"+
                "Capacity : "+tripsItems.get(position).getNumber()+" Person"

                +"\n"+
                "Price   : "+tripsItems.get(position).getPrice()+" EGP"  +"\n"+
                "Details   : "+tripsItems.get(position).getDetails()

        );
        holder.add.setText("Delete Trip");

        holder.delete.setVisibility(View.GONE);
        holder.add.setOnClickListener(view -> deleteTrip(view.getContext(),tripsItems.get(position).getId(),position));


    }

    private void deleteTrip(Context context,String id,int position ){
        tripId=id;
        AlertDialog.Builder alert = new AlertDialog.Builder(context);

        alert.setMessage("You want to cancel this booking");
        alert.setTitle("Cancel Trip");

        alert.setPositiveButton("Yes Delete", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                deleteTrip(position);
            }
        });

        alert.setNegativeButton("No Thanks", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                dialog.dismiss();
                dialog.cancel();
            }
        });

        alert.show();
    }




    public void deleteTrip(int position){

        BookTripRequest loginRequest = new BookTripRequest("delete_trip");
        loginRequest.setUserId(UserPreferenceHelper.getUserDetails().getId()+"");
        loginRequest.setTripId(tripId);

        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {

                DefaultResponse defaultResponse = (DefaultResponse) response;
                if(defaultResponse.getStatus()==101) {

                    Toast.makeText(context, "Booked Successfully", Toast.LENGTH_SHORT).show();
                    tripsItems.remove(position);
                    notifyDataSetChanged();
                }
            }

            @Override
            public void onRequestError(Object error) {

            }
        }).requestJsonObject(loginRequest, DefaultResponse.class);
    }

    @Override
    public int getItemCount() {
        return tripsItems==null?0:tripsItems.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        ImageView imageView;
        TextView name,details,add,delete;

        public ViewHolder(View itemView) {
            super(itemView);
            name=itemView.findViewById(R.id.name);
            details=itemView.findViewById(R.id.details);
            imageView=itemView.findViewById(R.id.image);
            add=itemView.findViewById(R.id.add);
            delete=itemView.findViewById(R.id.delete);
        }
    }
}
