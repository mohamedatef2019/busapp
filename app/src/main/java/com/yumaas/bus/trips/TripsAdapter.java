package com.yumaas.bus.trips;

import android.app.AlertDialog;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.text.Editable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.core.app.NotificationCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.yumaas.bus.R;
import com.yumaas.bus.base.DefaultResponse;
import com.yumaas.bus.base.UserPreferenceHelper;
import com.yumaas.bus.base.volleyutils.ConnectionHelper;
import com.yumaas.bus.base.volleyutils.ConnectionListener;
import com.yumaas.bus.drivers.response.DriversItem;
import com.yumaas.bus.noneed.SplashScreenActivity;
import com.yumaas.bus.trips.response.TripsItem;

import java.util.List;


public class TripsAdapter extends RecyclerView.Adapter<TripsAdapter.ViewHolder> {

    Context context;

    List<TripsItem> tripsItems;

    public TripsAdapter(Context context, List<TripsItem>tripsItems) {
        this.context = context;
        this.tripsItems=tripsItems;
    }


    @Override
    public TripsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_trip, parent, false);
        TripsAdapter.ViewHolder viewHolder = new TripsAdapter.ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(TripsAdapter.ViewHolder holder, final int position) {

        holder.name.setText(
                "Trip Name : "+tripsItems.get(position).getName()
                +"\n"+ "Driver Name : "+tripsItems.get(position).getDriver()
                        );

        holder.details.setText("From : "+tripsItems.get(position).getTripFrom()+"\n"+
                "To   : "+tripsItems.get(position).getTripTo()

                +"\n"+
                "Capacity : "+tripsItems.get(position).getNumber()+" Person"

                +"\n"+
                "Price   : "+tripsItems.get(position).getPrice()+" EGP"
                +"\n"+
                "Details   : "+tripsItems.get(position).getDetails()


        );


        if(UserPreferenceHelper.getUserDetails().getType().equals("2")){
            holder.delete.setVisibility(View.VISIBLE);
        }else {
            holder.delete.setVisibility(View.GONE);
        }

        holder.delete.setOnClickListener(view -> deleteTrip(view.getContext(),tripsItems.get(position).getId(),position));


        holder.add.setOnClickListener(view -> addComment(view.getContext()));


    }

    private void addComment(Context context){
        AlertDialog.Builder alert = new AlertDialog.Builder(context);
        final EditText edittext = new EditText(context);
        alert.setMessage("Enter Your Comment");
        alert.setTitle("Add Comment");

        alert.setView(edittext);

        alert.setPositiveButton("Send", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {

                String YouEditTextValue = edittext.getText().toString();

                sendNotification("Bus Travel",YouEditTextValue,context);

            }
        });

        alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                // what ever you want to do with No option.
            }
        });

        alert.show();
    }
    String tripId="0";
    private void deleteTrip(Context context,String id,int position ){
        tripId=id;
        AlertDialog.Builder alert = new AlertDialog.Builder(context);

        alert.setMessage("You want to cancel this booking");
        alert.setTitle("Cancel Trip");

        alert.setPositiveButton("Yes Delete", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                deleteTrip(position);
            }
        });

        alert.setNegativeButton("No Thanks", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                dialog.dismiss();
                dialog.cancel();
            }
        });

        alert.show();
    }




    public void deleteTrip(int position){

        BookTripRequest loginRequest = new BookTripRequest("company_delete_trip");
        loginRequest.setUserId(UserPreferenceHelper.getUserDetails().getId()+"");
        loginRequest.setTripId(tripId);

        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {

                DefaultResponse defaultResponse = (DefaultResponse) response;
                if(defaultResponse.getStatus()==101) {

                    Toast.makeText(context, "Booked Successfully", Toast.LENGTH_SHORT).show();
                    tripsItems.remove(position);
                    notifyDataSetChanged();
                }
            }

            @Override
            public void onRequestError(Object error) {

            }
        }).requestJsonObject(loginRequest, DefaultResponse.class);
    }


    private void sendNotification(String title, String body,Context context) {
        Intent intent = new Intent(context, SplashScreenActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0 /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT);

        String channelId = "channelId";
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder =
                new NotificationCompat.Builder(context, channelId)
                        .setSmallIcon(R.mipmap.ic_launcher)
                        .setContentTitle(title)
                        .setContentText(body)
                        .setAutoCancel(true)
                        .setSound(defaultSoundUri)
                        .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        // Since android Oreo notification channel is needed.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(channelId,
                    "Channel human readable title",
                    NotificationManager.IMPORTANCE_DEFAULT);
            notificationManager.createNotificationChannel(channel);
        }

        notificationManager.notify(0, notificationBuilder.build());
    }

    @Override
    public int getItemCount() {
        return tripsItems==null?0:tripsItems.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        ImageView imageView;
        TextView name,details,add,delete;

        public ViewHolder(View itemView) {
            super(itemView);
            name=itemView.findViewById(R.id.name);
            details=itemView.findViewById(R.id.details);
            imageView=itemView.findViewById(R.id.image);
            add=itemView.findViewById(R.id.add);
            delete=itemView.findViewById(R.id.delete);
        }
    }
}
